# Webship - Combining physics and multiplayer

## Aim

IO multiplayer browser games are becoming more and more popular, with more sophisticated games being produced all the time. Very few, if any, of these games have taken advantage of a fully physically enabled world, and no standard game engine exists.

Webship is capable of combining physics and multiplayer IO, opening up a world of possibilities for new games. It will also be a fully capable game engine, capable of loading project files and managing deployment from any server.

## Background

The Webship server is built in NodeJS, and the frontend rendered in React along with from-scratch canvas rendering. The game world environment is powered by MatterJS from the server; the client is only responsible for user interaction.

## Progress

Webship development is currently on hold due to time constraints, but will resume at a future date.