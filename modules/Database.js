const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const config = require('../config.json');

mongoose.connect(config.database.url, { useNewUrlParser: true });

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: String,
    email: String,
    pass: String,
});

/*
const UserSchema = new Schema({
    username: String,
    email: String,
    pass: String,
    in_wallet: {
        addr: String,
        priv_addr: String,
        balance: Number,
        paid: Number
    },
    out_wallet: String,
    debt: [
        {
            user: String,
            amount: Number
        }
    ]
});
UserSchema.statics.getById = function (id) {
    return new Promise((resolve, reject) => {
        this.findById(id, function (err, user) {
            if (err)
                reject(err);
            else
                resolve(user);
        });
    });
};
UserSchema.statics.getByName = function (name) {
    return new Promise((resolve, reject) => {
        this.findOne({username: new RegExp(`^${name}$`, "i")}, (err, user) => {
            if (err)
                reject(err);
            else
                resolve(user);
        });
    });
};
UserSchema.statics.userExists = function (name) {
    return new Promise((resolve, reject) => {
        this.count({username: new RegExp(`^${name}$`, "i")}, (err, count) => {
            if (err)
                reject(err);
            else
                resolve(count > 0);
        });
    });
};
UserSchema.methods.auth = function (password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, this.pass).then((res) => {
            resolve(res);
        }).catch (err => {
            reject(err);
        });
    });
};

const User = mongoose.model('User', UserSchema);

const AdSchema = new Schema ({
    title: String,
    content: String,
    logo: String,
    url: String,
    status: Number,
    user: String,
    stats: [{
        clicks: Number,
        day: Number
    }],
    ips: [{
        ip: String,
        expire: Date
    }],
    day_created: { type : Date, default: Date.now },
    price: Number,
    views_remaining: Number
});

const Ad = mongoose.model('Ad', AdSchema);

const PlatformSchema = new Schema({
    name: String,
    url: String,
    stats: [{
        ad_clicks: Number,
        day: Number
    }],
    day_created: { type : Date, default: Date.now },
    user: String
});

const Platform = mongoose.model('Platform', PlatformSchema);

module.exports.User = User;
module.exports.Ad = Ad;
module.exports.Platform = Platform;
*/