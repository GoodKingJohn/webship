import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import superagent from 'superagent';
import ConsoleInput from './ConsoleInput.js';
import './App.css';

const ListenerType = {Finite:0, Infinite:1};

class Listener {

    constructor (command, call, type, count = 1) {
        this.command = command;
        this.type = type;
        this.call = call;
        this.count = count;
    }

    trigger (data) {
        if (data.command === this.command) {
            if (this.count > 0 || this.type === ListenerType.Infinite) {
                this.call(data);
                if (this.type === ListenerType.Finite) {
                    this.count--;
                }
                return true;
            } else {
                return false;
            }
        }
    }

}

class LocalEntity {

    constructor (id, name, sprite, vertices, position, angle) {
        this.id = id;
        this.sprite = sprite;
        this.name = name;
        this.vertices = vertices;
        this.position = position;
        this.angle = angle;
        this.isInvisible = false;
    }

}

class App extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            pos: {x: 0.1, y: 0.1},
            variables: {},
            secret: "",
            sprites: {},
            entities: []
        };
        this.listeners = [];
        this.socket = new WebSocket('ws://localhost:8080');
        this.socket.onopen = () => {
            //this.socket.send("Hello!");
        };
        this.socket.onmessage = message => {
            let data = JSON.parse(message.data);
            let toRemove = [];
            this.listeners.forEach(listener => {
                if (!listener.trigger(data)) {
                    toRemove.push(listener);
                }
            });
            toRemove.forEach(listener => {
                this.listeners.remove(listener);
            });
        };
        this.socket.onclose = () => {
            alert("Socket closed.");
        }
    }

    render() {
        return (
            <Router>
                <div className="App">
                    <ConsoleInput
                        startText="Enter command"
                        textLimit="100"
                        minimized="true"
                        onCommand={(command) => this.handleCommand(command)}
                        ref={(element) => this.consoleInput = element}
                    />
                    <canvas width={400} height={400} className="shipDisplay" ref={(element) => this.display = element} />
                </div>
            </Router>
        );
    }

    displayRender () {
        let context = this.display.getContext("2d");
        context.clearRect(0, 0, this.display.width, this.display.height);
        this.state.entities.forEach(entity => {
            this.consoleInput.print(entity);
            /*let vertices = entity.vertices.splice();
            for (let i in vertices) {
                const vertex = vertices[i];
                vertex.x = vertex.x * Math.cos(entity.angle) - vertex.y * Math.sin(vertex.angle);
                vertex.y = vertex.x * Math.sin(entity.angle) + vertex.y * Math.cos(vertex.angle);
                vertex.x += entity.position.x;
                vertex.y += entity.position.y;
            }
            //this.consoleInput.print("Position updated " + JSON.stringify(bounds));
            context.beginPath();
            context.moveTo(vertices[0].x, vertices[0].y);
            vertices.forEach(vertex => {
                context.lineTo(vertex.x, vertex.y);
            });
            context.closePath();
            context.fillStyle = "#FF0000";
            context.fill();
            //this.consoleInput.print("Body: " + JSON.stringify(vertices));*/
        });
    }

    handleCommand (command) {
        let commands = command.split(' ');
        switch (commands[0]) {
            default:
                this.consoleInput.print("Unknown command '" + commands[0] + "'");
                break;
            case "":
                break;
            case "clear":
                this.consoleInput.clear();
                break;
            case "help":
                this.consoleInput.print("HELP!");
                break;
            case "time":
                this.command_Time();
                break;
            case "set":
                if (!commands[1] || !commands[2]) {
                    this.consoleInput.print("Useage: set <name> <value>");
                } else if (!isNaN(parseInt(commands[1]))) {
                    this.consoleInput.print("Variable name cannot be a number!");
                } else {
                    let cloneState = Object.assign({}, this.state);
                    cloneState.variables[commands[1]] = commands[2];
                    this.setState(cloneState);
                    this.consoleInput.print("Set variable " + commands[1]);
                }
                break;
            case "echo":
                this.command_Echo(command.slice(5));
                break;
            case "fetch":
                this.command_FetchShipData(command.slice(6));
                break;
            case "init":
                this.command_Init();
                break;
            case "stop":
                this.command_Stop();
                break;
            case "start":
                this.command_Start();
                break;
            case "join":
                this.command_Join(commands[1], Number.parseInt(commands[2]));
                break;
            case "launch":
                this.command_Launch(commands[1], commands[2], commands[3]);
                break;
            case "thrust":
                this.command_Thrust(Number.parseInt(commands[1], Number.parseInt(commands[2])));
                break;
            case "data":
                this.command_Data(commands.splice(1));
        }
    }

    command_Echo (str) {
        if (!str) {
            this.consoleInput.print("");
        } else {
            let regex = /%(.*?)%/g;
            let matches = str.match(regex);
            if (matches) {
                matches.forEach(val => {
                    let vn = val.replace(/%/g, "");
                    str = str.replace(val, this.state.variables[vn]);
                });
            }
            this.consoleInput.print(str);
        }
    }

    command_Time () {
        superagent.get('http://localhost:4000')
            .set('Accept', 'application/json')
            .end((err, res) => {
                if (err) {
                    return console.log(err);
                }
                let time = JSON.parse(res.text);
                this.consoleInput.print("Time: " + time.time + ", Day: " + time.day);
            });
    }

    command_FetchShipData (data_str) {
        let data_req;
        try {
            data_req = JSON.parse(data_str);
        } catch (e) {
            let print = ["Error parsing JSON string:"];
            print.push(e.message);
            this.consoleInput.print(print);
            return;
        }
        if (!data_req) return;
        superagent.get('http://localhost:4000/data-req')
            .query({data_req: data_req, secret:this.state.secret})
            .set('Accept', 'application/json')
            .end((err, res) => {
                if (err) {
                    return console.log(err);
                }
                this.consoleInput.print(res.text);
            });
    }

    command_Init () {
        this.socket.send(JSON.stringify({command:"update"}));
        this.listeners.push(new Listener("update", data => {
            if (!data.success || !data.updates) {
                return;
            }
            let updates = data.updates;
            updates.forEach(update => {
                const content = update.content;
                let cloneState = {};
                switch (update.type) {
                    case "newentity":
                        const entity = new LocalEntity(content.id, content.name, content.sprite, content.vertices, content.position, content.angle);
                        cloneState = Object.assign({}, this.state);
                        cloneState.entities[content.id] = entity;
                        //this.consoleInput.print(JSON.stringify(update));
                        this.setState(cloneState);
                        break;
                    case "pos":
                        //console.log("ID: " + content.id + " Entity: " + this.state.entities[content.id]);
                        cloneState = Object.assign({}, this.state);
                        cloneState.entities[content.id].position = content.position;
                        cloneState.entities[content.id].angle = content.angle;
                        this.setState(cloneState);
                        break;
                }
            });
            this.displayRender();
        }, ListenerType.Infinite));
    }

    command_Stop () {
        this.consoleInput.print("Stopping loop...", () => {
            superagent.post('http://localhost:4000')
                .send({stop:1})
                .then(res => {
                    if (JSON.parse(res.text).success)
                        this.consoleInput.print("Game loop stopped");
                }, err => {
                    this.consoleInput.print(err);
                });
        });
    }

    command_Start () {
        this.consoleInput.print("Starting loop...", () => {
            superagent.post('http://localhost:4000')
                .send({stop:0})
                .then(res => {
                    if (JSON.parse(res.text).success)
                        this.consoleInput.print("Game loop started");
                }, err => {
                    this.consoleInput.print(err);
                });
        });
    }

    command_Join (name, gameId) {
        this.consoleInput.print("Sending join request...", () => {
            this.socket.send(JSON.stringify({command:"createUser", name:name, gameId:gameId}));
            this.listeners.push(new Listener("createUser", data => {
                if (data.success) {
                    this.consoleInput.print("Created new user!");
                } else {
                    this.consoleInput.print(["Error...",JSON.stringify(data)]);
                }
            }, ListenerType.Finite));
        });
    }

    command_Launch (name, type, pass) {
        this.consoleInput.print("Creating ship...", () => {
            superagent.post('http://localhost:4000/create-ship')
                .send({name: name, type: type, pass: pass})
                .then(res => {
                    let resJSON = JSON.parse(res.text);
                    if (resJSON.success) {
                        this.consoleInput.print("Ship " + resJSON.name + " created with ID " + resJSON.id);
                    } else
                        this.consoleInput.print(["Error creating user:", resJSON.err]);
                }).catch(err => {
                    this.consoleInput.print(err);
                });
        });
    }

    command_Data (commands) {
        if (commands[0] === "bounds") {
            this.socket.send(JSON.stringify({command:"pos"}));
            this.listeners.push(new Listener("pos", data => {
                this.consoleInput.print(JSON.stringify(data));
            }, ListenerType.Finite));
        }
    }

    command_Thrust (x, y) {
        this.consoleInput.print("Thrusting...", () => {
            superagent.post('http://localhost:4000/add-force')
                .send({secret: this.state.secret, x: x, y: y})
                .then(res => {
                    let resJSON = JSON.parse(res.text);
                    if (resJSON.success) {
                        this.consoleInput.print("Thrusted!");
                    } else
                        this.consoleInput.print("An error occurred.");
                }).catch(err => {
                    this.consoleInput.print(err);
                });
        });
    }

}

export default App;

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};