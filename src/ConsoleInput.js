import React from "react";
import './ConsoleInput.css';

class ConsoleInput extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            minimized: this.props.minimized,
            text: [this.props.startText],
            history: [],
            history_index: 0,
            input_text: ""
        };
    }

    render () {
        if (!this.state.minimized) {
            const textLines = this.state.text.map((text, index) => {
                return (
                    <tbody key={index}>
                        <tr>
                            <td>
                                {text}
                            </td>
                        </tr>
                    </tbody>
                );
            });

            return (
                <div className="consoleBox">
                    <button className="minimizeBtn" onClick={() => this.handleMinimize()} />
                    <div className="consoleContainer">
                        <div className="console" id="consoleText" ref={element => this.consoleText = element}>
                            <table>
                                {textLines}
                            </table>
                        </div>
                        <br/>
                        <div className="consoleCtrls">
                            <input type="text" className="consoleInput" onKeyDown={this.handleKeyDown.bind(this)}
                                   ref={(element) => this.input = element}
                                   value={this.state.input_text || ""}
                                   onChange={this.handleInputChange.bind(this)}
                            />
                            <button className="consoleBtn" onClick={() => this.handleCommand()}>Execute</button>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="consoleBox">
                    <button className="minimizeBtn" onClick={() => this.handleMinimize()} />
                    <code>Console</code>
                </div>
            );
        }
    }

    handleInputChange(e){
        let cloneState = Object.assign({}, this.state);
        cloneState.input_text = e.target.value;
        this.setState(cloneState);
    }

    handleKeyDown (e) {
        if (e.key === "Enter") {
            this.handleCommand();
        }
        if (e.key === "ArrowUp") {
            let cloneState = Object.assign({}, this.state);
            cloneState.history_index++;
            if (cloneState.history_index > cloneState.history.length) cloneState.history_index--;
            cloneState.input_text = this.state.history[this.state.history.length - cloneState.history_index];
            this.setState(cloneState);
        }
        if (e.key === "ArrowDown") {
            let cloneState = Object.assign({}, this.state);
            cloneState.history_index--;
            if (cloneState.history_index < 0) cloneState.history_index++;
            cloneState.input_text = this.state.history[this.state.history.length - cloneState.history_index];
            this.setState(cloneState);
        }
    }

    handleCommand () {
        let command = this.input.value;
        if (command !== "") {
            let cloneState = Object.assign({}, this.state);
            cloneState.history.push(command);
            cloneState.input_text = "";
            cloneState.history_index = 0;
            this.setState(cloneState, () => {
                this.print("> " + command, () => {
                    this.props.onCommand(command);
                });
            });
        }
    }

    handleMinimize () {
        let cloneState = Object.assign({}, this.state);
        cloneState.minimized = !this.state.minimized;
        this.setState(cloneState);
    }

    print(text, callback) {
        let newText = this.state.text.slice();
        if (Array.isArray(text)) {
            text.forEach(val => {
                newText.push(val);
            });
        } else {
            newText.push(text);
        }
        if (this.props.textLimit) {
            if (newText.length > parseInt(this.props.textLimit)) {
                newText = newText.slice(1, newText.length);
            }
        }
        let cloneState = Object.assign({}, this.state);
        cloneState.text = newText;
        this.setState(
                cloneState,
                () => {
                    this.consoleText.scrollTop = this.consoleText.scrollHeight;
                    if (callback) callback();
                }
            );
    }

    clear () {
        let cloneState = Object.assign({}, this.state);
        cloneState.text = [];
        this.setState(cloneState);
    }

}

export default ConsoleInput;