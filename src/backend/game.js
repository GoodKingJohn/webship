var gameloop = require('node-gameloop');
var Matter = require('matter-js');
var bcrypt = require('bcrypt');
var fs = require('fs');
var path = require('path');

var Engine = Matter.Engine,
    World = Matter.World,
    Bodies = Matter.Bodies,
    Composite = Matter.Composite,
    Vertices = Matter.Vertices;

var games = [];

var sprites = [];

const saltRounds = 10;
const secretLength = 20;

const bodySizeTypes = Object.freeze({RECT: "rect", CIRCLE: "circle"});

function findAllInDirWithExt (dir, ext, done) {
    var results = [];
    fs.readdir(dir, function(err, list) {
        if (err) return done(err);
        var pending = list.length;
        if (!pending) return done(null, results);
        list.forEach(function(file) {
            file = path.resolve(dir, file);
            fs.stat(file, function(err, stat) {
                if (stat && stat.isDirectory()) {
                    findAllInDirWithExt(file, ext, function(err, res) {
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else if (path.extname(file) === '.' + ext) {
                    results.push(file);
                    console.log("Found " + file);
                    if (!--pending) done(null, results);
                } else {
                    if (!--pending) done(null, results);
                }
            });
        });
    });
};

class SpriteRegister {

    constructor () {
        this.count = 0;
        this.sprites = [];
    }

    register (spritePath, ref)
    {
        for (var element in this.sprites) {
            if (element.ref === ref) {
                return null;
            }
        }
        if (!fs.existsSync(spritePath)) {
            return null;
        }
        var sprite = new Sprite(spritePath, ref, this.count);
        console.log("New sprite " + JSON.stringify(sprite));
        this.sprites.push(sprite);
        console.log("Test: " + JSON.stringify(this.sprites));
        console.log("Test: " + JSON.stringify(this.sprites[this.count]));
        this.count ++;
        return sprite;
    }

}

var spriteRegister = new SpriteRegister();

class Sprite {

    constructor (spritePath, ref, id) {
        this.spritePath = spritePath;
        this.ref = ref;
        this.id = id;
    }

}

function getSprite (ref) {
    for (var i in spriteRegister.sprites) {
        var sprite = spriteRegister.sprites[i];
        if (sprite.ref === ref) {
            return sprite;
        }
    }
    return null;
}

function init () {

    //spriteRegister = new SpriteRegister();
    var spriteFiles = [];
    findAllInDirWithExt("resources/sprites", "png", (err, files) => {
        if (err) console.log(err);
        spriteFiles = files;
        spriteFiles.forEach(element => {
            var ref = path.basename(element, ".png").toLowerCase().replace(" ", "");
            var sprite = spriteRegister.register(element, ref);
            console.log("Registered new sprite " + sprite.ref);
            console.log(JSON.stringify(spriteRegister.sprites));
        });
    });

}

class GameInstance {

    constructor (id) {
        this.id = id;
        this.dataManager = new DataManager(this);
        this.world = World.create({bounds: {min: {x: 0, y: 0}, max: {x: 400, y: 400}}});
        this.engine = Engine.create({world: this.world});
        this.entityRegister = new EntityRegister(this.engine, this.dataManager);
        this.world.gravity.y = 100000;

        var bottomWall = this.entityRegister.createRectangleEntity(200, 390, 400, 20, "bottomWall", {isStatic: true});
        var leftWall = this.entityRegister.createRectangleEntity(10, 200, 20, 400, "leftWall", {isStatic: true});
        var rightWall = this.entityRegister.createRectangleEntity(390, 200, 20, 400, "rightWall", {isStatic: true});

        var prevDelta = 1;

        var engineVar = this.engine;
        var dataManagerVar = this.dataManager;

        this.loopId = gameloop.setGameLoop(function (delta) {
            // `delta` is the delta time from the last frame
            Engine.update(engineVar, delta, delta / prevDelta);
            dataManagerVar.cycle();
            prevDelta = delta;
        }, 1000 / 60); // run at 60fps
    }

    stop () {
        gameloop.clearGameLoop(this.loopId);
    }

}

function newLoop (users, errCallback) {
    console.log("Starting loop!");
    var gameInstance = new GameInstance(games.length);
    games.push(gameInstance);
    if (users)
        users[gameInstance.id] = [];
}

function initUser (user) {
    var register = games[user.gameId].entityRegister;
    register.createCircleEntity(200, 200, 20, user.name);
}

class Entity {

    constructor (id, name, body, sprite, scripts, parent, dataManager) {
        this.id = id;
        this.body = body;
        this.scripts = scripts;
        this.sprite = sprite;
        this.parent = parent;
        this.isInvisible = false;
        this.dataManager = dataManager;
        this.name = name;
    }

    dataInit () {
        var vertices = [];
        this.body.vertices.forEach(vertex => {
            vertices.push({x:vertex.x, y:vertex.y, index:vertex.index});
        });
        Vertices.translate(vertices, {x:-this.body.position.x, y:-this.body.position.y}, 1);
        Vertices.rotate(vertices, -this.body.angle, {x:0, y:0});
        return {type:"newentity",content:{position:this.body.position, angle:this.body.angle, vertices:vertices, id:this.id, sprite:this.sprite, name:this.name}};
    }

    dataState () {

    }

    dataUpdate () {
        return {type:"pos",content:{position:this.body.position, angle:this.body.angle, id:this.id}};
    }

}

class EntityRegister {

    constructor (engine, dataManager) {
        this.entities = [];
        this.engine = engine;
        this.dataManager = dataManager;
    }

    createEmptyEntity (x, y, name, parent, scripts = []) {
        var body = Bodies.rectangle(x, y, 1, 1, {isStatic: true, isSensor: true, parent:(parent ? parent.body : null)});
        var entity = new Entity(this.entities.length, name, body, scripts, parent, this.dataManager);
        entity.isInvisible = true;
        World.add(this.engine.world, body);
        this.entities.push(entity);
        return entity;
    }

    createCircleEntity (x, y, radius, name, options, parent, scripts = []) {
        var optionsObj = options ? options : {};
        optionsObj.parent = (parent ? parent.body : null);
        var body = Bodies.circle(x, y, radius, optionsObj);
        var entity = new Entity(this.entities.length, name, body, scripts, parent, this.dataManager);
        World.add(this.engine.world, body);
        this.entities.push(entity);
        return entity;
    }

    createRectangleEntity (x, y, width, height, name, options, parent, scripts = []) {
        var optionsObj = options ? options : {};
        optionsObj.parent = (parent ? parent.body : null);
        var body = Bodies.rectangle(x, y, width, height, optionsObj);
        var entity = new Entity(this.entities.length, name, body, null, scripts, parent, this.dataManager);
        World.add(this.engine.world, body);
        this.entities.push(entity);
        return entity;
    }

    createCustomEntity (body, name, parent, scripts = []) {
        var entity = new Entity(this.entities.length, name, body, scripts, parent, this.dataManager);
        World.add(this.engine.world, body);
        this.entities.push(entity);
        return entity;
    }

    getEntity (id) {
        return this.entities[id];
    }

}

class EntityScript {

    constructor (init, update, entity) {
        this.init = init;
        this.update = update;
        this.entity = entity;
    }

}

function getGameInstance (id) {
    return games[id];
}

function getEntities(id) {
    return games[id].entityRegister.entities;
}

function getEntityRegister (id) {
    return games[id].entityRegister;
}

function getBodies (id) {
    return Composite.allBodies(games[id].world);
}

class DataManager {
    constructor (gameInstance) {
        this.updates = [];
        this.listeners = [];
        this.instance = gameInstance;
    }

    addListener (listener) {
        console.log("Adding listener...");
        this.listeners.push(listener);
        if (listener.initDump) {
            console.log("Init dumping...");
            var initUpdates = []
            this.instance.entityRegister.entities.forEach(entity => {
                initUpdates.push(entity.dataInit());
            });
            console.log("Sending " + JSON.stringify(initUpdates));
            listener.send(initUpdates);
            listener.initDump = false;
        }
    }

    listen (data) {
        this.updates.push(data);
    }

    cycle () {
        for (var i in this.instance.entityRegister.entities) {
            var entity = this.instance.entityRegister.entities[i];
            this.listen(entity.dataUpdate());
            var stateUpdate = entity.dataState();
            if (stateUpdate) {
                this.listen(stateUpdate);
            }
        }
        if (this.updates.length > 0) {
            this.flush();
            return true;
        }
        return false;
    }

    flush () {
        for (var i in this.listeners) {
            this.listeners[i].send(this.updates);
        }
        this.updates = [];
    }
}

class DataListener {
    constructor (socket) {
        this.socket = socket;
        this.initDump = true;
    }

    send (updates) {
        this.socket.send(JSON.stringify({command:"update",updates:updates,success:1}));
    }
}

function addDataListener (listener, gameId) {
    getGameInstance(gameId).dataManager.addListener(listener);
}

module.exports.newLoop = newLoop;
module.exports.getEntities = getEntities;
module.exports.getBodies = getBodies;
module.exports.initUser = initUser;
module.exports.Entity = Entity;
module.exports.EntityRegister = EntityRegister;
module.exports.init = init;
module.exports.getSprite = getSprite;
module.exports.spriteRegister = spriteRegister;
module.exports.DataListener = DataListener;
module.exports.addDataListener = addDataListener;