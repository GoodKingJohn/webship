var express = require('express');
var router = express.Router();
//var Database = require("../../modules/Database");
var game = require('./game');

router.get('/', async (req, res) => {

});

router.get('/sprite', async (req, res) => {
    var ref = req.query.ref;
    console.log("Sprite " + ref + " requested");
    console.log(game.spriteRegister.sprites);
    var sprite = game.getSprite(ref);
    if (!sprite) {
        console.log("Sprite not found");
        res.sendFile(__dirname + "/internal-resources/404.png", {}, function (err) {
            if (err) {
                console.log(err);
                res.status(err.status).end();
            }
        });
        return;
    }
    res.sendFile(sprite.spritePath, {}, function (err) {
        if (err) {
            console.log(err);
            res.status(err.status).end();
        }
        else {
            console.log('Sent: ', sprite.spritePath);
        }
    });
});

module.exports = router;
