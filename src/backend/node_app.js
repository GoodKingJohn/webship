var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var cors = require('cors');
var bodyParser = require('body-parser');
var game = require('./game');

var index = require('./index');
var node_app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
node_app.use(cors());
node_app.use(bodyParser.json());
node_app.use(bodyParser.urlencoded({extended: false}));
node_app.use(cookieParser());
node_app.use(express.static(path.join(__dirname, 'public')));

// ROUTES
node_app.use('/', index);

// catch 404 and forward to error handler
node_app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
node_app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500).end();
    //res.render('error');
});

module.exports = node_app;
