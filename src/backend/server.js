#!/usr/bin/env node

/**
 * Module dependencies.
 */

var game = require('./game');
var DataListener = game.DataListener;

var app = require('./node_app');
var http = require('http');
var WebSocket = require('ws');

/**
 * Initialise user store object
 */

var users = [];

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '4000');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Get Port for WebSocket server
 */

var wsPort = normalizePort(process.env.PORT || '8080');

/**
 * Create WebSocket server
 */

var socket = new WebSocket.Server({port: wsPort});

socket.on('connection', connection => {
    connection.on('message', message => {
        console.log("Received " + message);
        var data = JSON.parse(message);
        switch (data.command) {
            case "createUser":
                if (users[data.gameId]) {
                    connection.user = {};
                    connection.user.name = data.name;
                    connection.user.gameId = data.gameId;
                    game.initUser(connection.user);
                    connection.send(JSON.stringify({command:"createUser", success:1}));
                }
                break;
            case "pos":
                console.log("Positions requested");
                if (!connection.user) {
                    connection.send(JSON.stringify({command:"pos", success:0, err:"User not created!"}));
                }
                setInterval(() => {
                    var bodies = game.getBodies(connection.user.gameId);
                    var positions = [];
                    bodies.forEach(body => {
                        var vertices = [];
                        body.vertices.forEach(vertex => {
                            vertices.push({x:vertex.x, y:vertex.y, index:vertex.index});
                        });
                        positions.push({vertices:vertices, bounds:body.bounds});
                    });
                    connection.send(JSON.stringify({command:"pos", success:1, bodies:positions}));
                }, 20);
                break;
            case "update":
                if (!connection.user) {
                    connection.send(JSON.stringify({command:"update", success:0, err:"User not created!"}));
                }
                var listener = new DataListener(connection);
                game.addDataListener(listener, connection.user.gameId);
                break;
        }
    });
});

/**
 * Initialise game
 */

game.init();
game.newLoop(users, e => {
    console.log("Error initiating loop: " + e);
});

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log("Server running on port " + port);
}